PREFIX = /usr
BINDIR = $(PREFIX)/bin

all:
	echo "Nothing to do, run make install"

install:
	install -D -m 0755 ./shellcheck.sh $(DESTDIR)/$(BINDIR)/shellcheck-online

termux-pkg:
	# pip3 install termux-create-package
	termux-create-package termux-pkg.json
