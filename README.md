# nixscript-shellcheck

![Screenshot](shellcheck-screen.png)

## Russian

**Termux** не поддкрживает **haskell**, поэтому
**shellcheck** установить не возможно.

Данный скрипт позволит обойти эту проблему.
Он отправляет скрипт на сервер **bash.tuxnix.ru**,
где установлен **shellcheck**, там происходит
проверка скрипта, и вы на **stdout** получаете
всё то, что **shellcheck** вывел, а ваш
отправленный скрипт удаляется с сервера сразу
после проверки **shellcheck**.

для удобства, на **bash.tuxnix.ru** клонирована
**shellcheck.wiki**, а скрипт по коду ошибки
выдаст вам markdown документ прямо в **stdout**.

```
shellcheck.sh script.sh
shellcheck.sh --code SC2029
shellcheck.sh --help
shellcheck.sh -h
```

Не забудьте дать права на исполнение

```
chmod +x shellcheck.sh
```

## English

**Termux** does not support **haskell**,
therefore **shellcheck** cannot be installed.

This script will work around this problem.
It sends the script to the server **bash.tuxnix.ru**,
where **shellcheck** is installed,
the script is checked there, and you get **stdout**
everything that **shellcheck** output, and the sent
script is deleted from the server immediately
after checking **shellcheck**.

For convenience, on **bash.tuxnix.ru** cloned
**shellcheck.wiki**, and the script for the
error code will give you a markdown document
directly to **stdout**.

```
shellcheck.sh script.sh
shellcheck.sh --code SC2029
shellcheck.sh --help
shellcheck.sh -h
```

Do not forget to give permissions to execute

```
chmod + x shellcheck.sh
```
