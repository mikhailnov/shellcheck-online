#!/bin/sh

set -e # fail on any error
set -f # Prevent file globbing

help() {
	echo "
Usage:
  $0 --code SCXXXX
  $0 --code scXXXX
  $0 --code XXXX
      Show markdown decription of error code
  $0 path_to_file
      Send file to the server for online shellcheck
  $0 --help
      This screen
  
	"
}

if [ -z "$*" ]; then
	help
	exit 1
fi

while [ -n "$1" ]
do
	case "$1" in
		--code )
			shift;
			if [ -n "$1" ]
				then
					input="$(echo "$1" | tr '[:lower:]' '[:upper:]')"
					# TODO: improve detecting correct and incorrect arguements
					if ! echo "$input" | grep -q "^SC[0-9]" && \
						echo "$input" | grep -q "^[0-9]"
						then input="SC${input}"
					fi
					curl "https://bash.tuxnix.ru/shellcheck.wiki/${input}.md"
				else
					help
					exit 1
			fi
		;;
		--help | -h )
			help
			exit
		;;
		*)
			if [ -f "$*" ] && [ -r "$*" ]
				then
					curl -X POST --form datafile=@"$*" -k https://bash.tuxnix.ru/shellcheck.php
				else
					echo "File $* does not exist, is not a file or is not readable"
					exit 1
			fi
		;;
	esac
	shift
done

